<?php

/**
 * Implements hook_features_api().
 */
function features_role_permission_features_api() {
  return array(
    'role_permission' => array(
      'name' => t('Role Permissions'),
      'feature_source' => TRUE,
      'default_hook' => 'default_role_permissions',
      'default_file' => FEATURES_DEFAULTS_INCLUDED,
      'file' => drupal_get_path('module', 'features_role_permission') . '/features_role_permission.features.inc',
    ),
  );
}

/**
 * Implements hook_features_export_options().
 */
function role_permission_features_export_options() {
  // Get sorted list of modules having permissions.
  $modules = array();
  $module_info = system_get_info('module');
  foreach (module_implements('permission') as $module) {
    $modules[$module] = $module_info[$module]['name'];
  }
  ksort($modules);

  // Build option for each role/permission combination.
  $roles = _features_role_permission_get_roles();
  $options = array();
  foreach ($roles as $role) {
    foreach ($modules as $module => $module_name) {
      if ($permissions = module_invoke($module, 'permission')) {
        foreach ($permissions as $perm => $perm_item) {
          _user_features_change_term_permission($perm);
          $options["$role:$perm"] = strip_tags("{$role}: {$module_name}: {$perm_item['title']}");
        }
      }
    }
  }

  return $options;
}

/**
 * Implements hook_features_export().
 */
function role_permission_features_export($data, &$export, $module_name) {
  $export['dependencies']['features'] = 'features';
  $export['dependencies']['features_role_permission'] = 'features_role_permission';

  $modules = user_permission_get_modules();
  $role_map = features_get_default_map('user_role', 'name');

  foreach ($data as $name) {
    list($role, $permission) = explode(':', $name);

    // Add dependencies for modules providing each permission.
    _user_features_change_term_permission($permission, 'machine_name');
    if (isset($modules[$permission])) {
      $export['dependencies'][$modules[$permission]] = $modules[$permission];
    }

    // Add dependency to module providing the role.
    if (isset($role_map[$role])) {
      $export['dependencies'][$role_map[$role]] = $role_map[$role];
    }

    $export['features']['role_permission'][$name] = $name;
  }
}

/**
 * Implements hook_features_export_render().
 */
function role_permission_features_export_render($module, $data) {
  $modules = &drupal_static(__FUNCTION__ . 'modules');
  if (!isset($modules)) {
    $modules = user_permission_get_modules();
  }

  $code[] = '  $role_permissions = array();';
  $code[] = '';

  $role_permissions = _user_features_get_permissions(FALSE);

  foreach ($data as $name) {
    list($export['role'], $export['permission']) = explode(':', $name);

    $permission = $export['permission'];
    _user_features_change_term_permission($permission, 'machine_name');

    if (isset($modules[$permission])) {
      $export['module'] = $modules[$permission];
    }

    $export['grant'] = !empty($role_permissions[$export['role']][$permission]);

    $code[] = "  // Exported role permission: $name.";
    $code[] = "  \$role_permissions['$name'] = " . features_var_export($export, '  ') . ";";
    $code[] = '';
  }

  $code[] = "  return \$role_permissions;";
  $code = implode("\n", $code);

  return array('default_role_permissions' => $code);
}

/**
 * Implements hook_features_revert().
 */
function role_permission_features_revert($module) {
  role_permission_features_rebuild($module);
}

/**
 * Implements hook_features_rebuild().
 */
function role_permission_features_rebuild($module) {
  if ($defaults = features_get_default('role_permission', $module)) {
    $modules = user_permission_get_modules();
    $roles = _features_role_permission_get_roles();

    // Build list of roles and permissions to change.
    $roles_to_change = array();
    foreach ($defaults as $role_permission) {
      $permission = $role_permission['permission'];
      _user_features_change_term_permission($permission, 'machine_name');
      if (empty($modules[$permission])) {
        drupal_set_message(t('Warning in features rebuild of !module. No module defines permission "!name".', array(
          '!module' => $module,
          '!name' => $permission,
        )), 'warning');
        continue;
      }

      $rid = array_search($role_permission['role'], $roles);
      $roles_to_change[$rid][$permission] = $role_permission['grant'];
    }

    // Write the updated permissions.
    foreach ($roles_to_change as $rid => $permissions) {
      user_role_change_permissions($rid, $permissions);
    }
  }
}

/**
 * Returns array of roles ($rid => $name) with role names untranslated.
 */
function _features_role_permission_get_roles($builtin = TRUE) {
  $roles = array();
  foreach (user_roles() as $rid => $name) {
    switch ($rid) {
      case DRUPAL_ANONYMOUS_RID:
        if ($builtin) {
          $roles[$rid] = 'anonymous user';
        }
        break;
      case DRUPAL_AUTHENTICATED_RID:
        if ($builtin) {
          $roles[$rid] = 'authenticated user';
        }
        break;
      default:
        $roles[$rid] = $name;
        break;
    }
  }
  return $roles;
}
